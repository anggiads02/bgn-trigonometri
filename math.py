#PROGRAM MATEMATIKA SEDERHANA

def lp(x):
    l=2*x
    return l
def kp(x):
    k=4*x
    return k
def lpp(x,y):
    l=x*y
    return l
def kpp(x,y):
    k=2*(x+y)
    return k
def ls(x,y):
    l=0.5*x*y
    return l
def ks(x):
    k=x+x+x
    return k
def ljg(x,y):
    l=x*y
    return l
def kjg(x,y):
    k=2*(x+y)
    return k
def ll(x):
    l=3.14*x*x
    return l
def kl(x):
    k=2*3.14*x
    return k
pilihan = 1
while pilihan !=0:
    print("MATEMATIKA SEDERHANA")
    print("1. Luas & Keliling Persegi")
    print("2. Luas & Keliling Persegi Panjang")
    print("3. Luas & Keliling Segitiga")
    print("4. Luas & Keliling Jajar Genjang")
    print("5. Luas & Keliling Lingkaran")
    print("0. Exit")
    
    pilihan=int(input("Masukkan pilihan: "))
    if pilihan==1:
        print("\n")
        print("Luas & Keliling Persegi")
        x=int(input("Sisi (cm): "))
        print("Luas  Persegi: %d \n" %lp(x))
    elif pilihan==2:
        print("\n")
        print("Luas & Keliling Persegi Panjang")
        x=int(input("Panjang (cm): "))
        y=int(input("Lebar: "))
        print("Luas Persegi Panjang: %d \n" %lpp(x,y))
        print("Keliling Persegi Panjang: %d \n" %kpp(x,y))
    elif pilihan==3:
        print("\n")
        print("Luas & Keliling Segitiga")
        x=int(input("Alas (cm): "))
        y=int(input("Tinggi (cm): "))
        print("Luas Segitiga: %d \n" %lpp(x,y))
        print("Keliling Segitiga: %d \n" %ks(x))
    elif pilihan==4:
        print("\n")
        print("Luas & Keliling Jajar Genjang")
        x=int(input("Sisi alas (cm): "))
        y=int(input("Tinggi/Sisi miring (cm): "))
        print("Luas Jajar Genjang: %d \n" %ljg(x,y))
        print("Keliling Jajar Genjang: %d \n" %kjg(x,y))
    elif pilihan==5:
        print("\n")
        print("Luas & Keliling Lingkaran")
        x=int(input("Jari-jari (cm): "))
        print("Luas Lingkaran: %.2f \n" %ll(x))
        print("Keliling Lingkaran: %.2f \n" %kl(x))
    else: 
        break

    